using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ladder : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Shaun")
        {
            int Gold = GameManager.instance.Gold;
            int potions = GameManager.instance.potions;
            PlayerPrefs.SetInt("Gold", Gold);
            PlayerPrefs.SetInt("potions", potions);
            PlayerPrefs.Save();
            SceneManager.LoadScene("BossRoom");
        }
    }
}
