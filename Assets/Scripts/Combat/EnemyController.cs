using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum EnemyState
{
    idle,
    follow,
    die,
};

public class EnemyController : MonoBehaviour
{
    public int currentHealth;
    public int maxHealth;
    public int pesosamount = 5;

    GameObject Player;
    public EnemyState currState = EnemyState.idle;

    public float range;
    public float speed;

    [SerializeField]

    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }

    private void Update()
    {
        switch(currState)
        {
            case (EnemyState.idle):
                Idle();
                break;
            case (EnemyState.follow):
                Follow();
                break;
            case (EnemyState.die):
                Die();
                break;
        }
    }

    public void HurtEnemy(int damage)
    {
        currentHealth -= damage;
        FindObjectOfType<AudioManager>().Play("SkeletonHit");
        GameManager.instance.ShowText("-" + damage, 25, Color.red, transform.position, Vector3.up * 40, 1.0f);
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
            GameManager.instance.Gold += pesosamount;
            GameManager.instance.ShowText("+" + pesosamount + " Gold", 30, Color.yellow, transform.position, Vector3.up * 40, 1.0f);

            currState = EnemyState.die;
        }

        if(IsPlayerInRange(range) && currState != EnemyState.die)
        {
            currState = EnemyState.follow;
        }
    }

    private bool IsPlayerInRange(float range)
    {
        return Vector3.Distance(transform.position, Player.transform.position) <= range;
    }

    private void Idle()
   {
        if (IsPlayerInRange(range))
        {
            currState = EnemyState.follow;
        }
   }

    private void Follow()
    {
        transform.position = Vector2.MoveTowards(transform.position, Player.transform.position, speed * Time.deltaTime);
    }

    private void Die()
    {

    }
}
