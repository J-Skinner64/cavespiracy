using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Hurtplayer : MonoBehaviour
{
    private HealthManager healthMan;
    public float Cooldown = 1f;
    public bool isTouching;
    [SerializeField]
    private int Damage = 2;

    // Start is called before the first frame update
    void Start()
    {
        healthMan = FindObjectOfType<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isTouching)
        {
            Cooldown -= Time.deltaTime;
            if (Cooldown <= 0)
            {
                healthMan.HurtPlayer(Damage);
                Cooldown = 1f;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.collider.tag == "Player")
        {
            other.gameObject.GetComponent<HealthManager>().HurtPlayer(Damage);
        }
    }

    private void OnCollisionStay2D(Collision2D other)
    {
        if (other.collider.tag == "Player")
        {
            isTouching = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.collider.tag == "Player")
        {
            isTouching = false;
            Cooldown = 2f;
        }
    }
}
