using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class HealthManager : MonoBehaviour
{
    public int currentHealth;
    public int maxHealth;
    public int potionHealth = 10;


    [SerializeField]


    public GameObject DeathScreen;
    public GameObject Music;

    void Start()
    {
        DeathScreen.SetActive(false);
    }

    private void Update()
    {
        if(Input.GetKeyDown("q"))
        {
            if (GameManager.instance.potions <= 0)
                return;
            else
            {
                for (int i = currentHealth; i < 10; i++)
                {
                    currentHealth++;
                }
                GameManager.instance.potions--;
                FindObjectOfType<AudioManager>().Play("Drinking");
            }
            
            
        }
    }


    public void HurtPlayer(int Damage)
    {
        currentHealth -= Damage;
        FindObjectOfType<AudioManager>().Play("PlayerHurt");

        if (currentHealth <= 0)
        {
            DeathScreen.SetActive(true);
            Music.SetActive(true);
        }

    }


}
