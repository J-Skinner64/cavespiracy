using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour
{
    public int Damage = 2;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name != "Player")
        {
            if (other.tag == "Enemy")
            {
                if(other.name == "Dragon")
                {
                    Dragom dragom;
                    dragom = other.gameObject.GetComponent<Dragom>();
                    dragom.HurtEnemy(Damage);
                }
                else
                {
                    EnemyController enemyManager;
                
                    enemyManager = other.gameObject.GetComponent<EnemyController>();              
                    enemyManager.HurtEnemy(Damage);
                }
            }
        }
    }
}
