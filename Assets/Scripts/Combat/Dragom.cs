using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Dragom : MonoBehaviour
{
    //Visuals
    public GameObject Fire;
    GameObject Player;
    public Slider bossHealth;
    private Animator Anim;
    public GameObject EndScreen;
    public GameObject HUD;

    public static Dragom instance;


    public int currentHealth;
    public int maxHealth;
    public int GoldAmount = 500;
    [SerializeField]

    private bool isAttacking = false;
    private bool canMove = true;
    private float count = 8f;


    void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        Anim = GetComponent<Animator>();
        bossHealth.maxValue = maxHealth;
        instance = this;
    }

    private void Update()
    {
        bossHealth.value = currentHealth;

        if(count <= -1)
        {
            count = 8f;
        }

        if (BossRoomController.instance.BossActive == true)
        {

            if(canMove == true)
            {
                 transform.position = Vector2.Lerp(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x, Player.transform.position.y), Time.deltaTime);
            }

            if (isAttacking == false)
            {
                canMove = true;
                Fire.SetActive(false);
                count -= Time.deltaTime;
                if (count <= 0)
                {
                    isAttacking = true;
                }
            }
            
           if(isAttacking == true)
           {
                canMove = false;
                FindObjectOfType<AudioManager>().Play("Fire");
                 Fire.SetActive(true);
                 Anim.SetTrigger("Attack");
                count -= Time.deltaTime;
                 if(count <= 5)
                 {
                    isAttacking = false;
                 }
           }
            
        } 

    }

    public void HurtEnemy(int damage)
    {
        currentHealth -= damage;
        GameManager.instance.ShowText("-" + damage, 25, Color.red, transform.position, Vector3.up * 40, 1.0f);
        if (currentHealth <= 0)
        {
            GameManager.instance.Gold += GoldAmount;
            GameManager.instance.ShowText("+" + GoldAmount + " Gold", 30, Color.yellow, transform.position, Vector3.up * 40, 1.0f);
            EndScreen.SetActive(true);
            HUD.SetActive(false);
            Destroy(gameObject);


        }
    }
}
