using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordAnimation : MonoBehaviour
{

    private Animator Anim;

    private float attackTime = .1f;
    private float attackCounter = .1f;
    private bool isAttacking;

    private void Start()
    {
        Anim = GetComponent<Animator>();
    }

    private void Update()
    {
         if (isAttacking)
         {
            attackCounter -= Time.deltaTime;
            if (attackCounter <= 0)
            {
                isAttacking = false;
            }
         }

         if (Input.GetKeyDown(KeyCode.Space))
         {
            attackCounter = attackTime;
            Anim.SetTrigger("Swing");
            isAttacking = true;
            FindObjectOfType<AudioManager>().Play("SwordSwing");
         }
        
    }

}
