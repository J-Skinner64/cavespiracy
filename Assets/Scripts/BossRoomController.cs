using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoomController : MonoBehaviour
{
    public GameObject bossUI;
    public GameObject Lights;
    public GameObject Barrier;
    public bool BossActive;

    public static BossRoomController instance;



    private void Start()
    {
        instance = this;
        GameObject.Find("Shaun");
        GameObject.Find("Main Camera");
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Shaun")
        {
            Debug.Log("Player has entered the boss battle!");
            bossUI.SetActive(true);
            Lights.SetActive(true);
            Barrier.SetActive(true);
            BossActive = true;           
        }
    }
}
