using UnityEngine;
using UnityEngine.UI;
public class BossRandomName : MonoBehaviour
{
    public Text nameText;
    private string[] names = new string[] { "Peter", "Ron", "Jeff", "Gary", "Trevor", "Dave", "Bob", "Michael", "Sean", "Juan", "Franklin", "Matto", "Larry", "Terry", "Jack", "Chris","Henry", "Steve", "Jake", "Liam", "Luke", "Connor", "Alex", "Joe", "Dylan", "Charlie", "Jamie", "", "Aaron", "Stuart", "Matt", "Todd", "Derek", "Eric", "Wayne", "Dwayne","Uncle Sam","Vladimir","Donald","Josh","Noah","John","Aiden","Rocky","Edgar","Benji","James","Oliver","Bobbry","Pablo", "Nick", "Clarence", "Noah", "Joseph", "Jeremiah","Johnathon","Tim","Malcolm","Leo","Steven", "Ivan", "Richard", "George", "Andrew", "Louis", "Adolf", "Otto", "Frank", "Chad"}; 
    private string[] adjectives = new string[] {"Starving", "Clumsy", "Lost", "Tired", "Diseased", "Giant", "Small", "Annoyed", "Confused", "Vengeful", "Young", "Elder", "Useless", "God-Like", "Lame", "", "Unholy","Unlucky", "Legendary", "Blind", "Fierce","Hippocrite", "Shy", "Hopeless", "Great", "Terrible", "Evil", "Gullible", "Heroic", "Villainous", "Unpleasant", "", "Sly", "Forgettable", "Forgotten", "Well-Known", "Hurtful", "Feared", "Victorious", "Cannibal","Repulsive","Obnoxious","Invisible","Ancient","Humble","Lonely","Crippled","Unknown", "Dinosaur", "Rock", "Boring", "Unfortunate", "Fortunate One", "Patriot", "Distressed","I","II","III","IV","V","VI","VII","VIII","IX","X", "Invincible", "Incompetent", "Fast", "Benevolent","Furious", "Invader", "Dragon", "Disengenuous", "Unstoppable", "Wanderer", "Paranoid", "child", "Coward", "Gluttonous"};
    public string GetRandomName()
    {
        return names[Random.Range(0, names.Length)];
    }

    public string GetRandomAdjective()
    {
        return adjectives[Random.Range(0, adjectives.Length)];
    }


    void Start()
    {
        nameText.text = GetRandomName() + " The " + GetRandomAdjective();
    }
}
