using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossUI : MonoBehaviour
{
    public ContactFilter2D filter;
    public BoxCollider2D boxCollider;
    private Collider2D[] hits = new Collider2D[10];
    public GameObject bossui;

    protected virtual void Start()
    {
        boxCollider = GetComponent<BoxCollider2D>();
        
    }

    protected virtual void Update()
    {
        //Collision Work
        boxCollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;

            OnCollide(hits[i]);

            hits[i] = null;
        }

    }

    protected virtual void OnCollide(Collider2D coll)
    {
        if(coll.name == "Shaun")
        {
            bossui.SetActive(true);
        }
        
    }
}
