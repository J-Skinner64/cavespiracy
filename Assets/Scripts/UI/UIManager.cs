using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private HealthManager healthMan;
    public Slider healthBar;
   
    public Text score;
    public Text potions;
    public Text Health;
    // Start is called before the first frame update
    void Start()
    {
        healthMan = FindObjectOfType<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {
        healthBar.maxValue = healthMan.maxHealth;
        healthBar.value = healthMan.currentHealth;
        int Score = GameManager.instance.Gold;
        score.text = Score.ToString();
        int Potions = GameManager.instance.potions;
        potions.text = Potions.ToString();
        Health.text = healthMan.currentHealth + "/10";
    }
}
