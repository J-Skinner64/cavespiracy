using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        PlayerPrefs.DeleteAll();
    }

    public void QuitGame()
    {
        PlayerPrefs.DeleteAll();
        Debug.Log("Quit");
        Application.Quit();
    }

    public void mainmenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void Restart()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void NextDungeon()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("BasementMain");
    }
}
