using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerationData : MonoBehaviour
{
    public int numberOfCrawlers;
    public int iterationMin;
    public int iterationMax;
}
