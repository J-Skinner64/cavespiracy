using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance;
    public Room currRoom;
    public float moveSpeed;

    void Awake()
    {
        instance = this;
    }
    private void Update()
    {
        UpdatePosition();
    }
    void UpdatePosition()
    {
        if (currRoom == null)
        {
            return;
        }

        Vector3 TargetPos = GetCameraTargetPosition();

        transform.position = Vector3.MoveTowards(transform.position, TargetPos, Time.deltaTime * moveSpeed);
    }

    Vector3 GetCameraTargetPosition()
    {
        if(currRoom == null)
        {
            return Vector3.zero;
        }
        Vector3 TargetPos = currRoom.GetRoomCentre();
        TargetPos.z = transform.position.z;

        return TargetPos;
    }

    public bool isswitchingscene()
    {
        return transform.position.Equals(GetCameraTargetPosition()) == false;
    }
}
