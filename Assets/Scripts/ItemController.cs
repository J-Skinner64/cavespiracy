using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    public string name;
    public string description;
    public Sprite itemImage;
}


public class ItemController : MonoBehaviour
{

    public Item item;
    public float healthChange;
    public float DamgeIncrease;


    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = item.itemImage;
    }
}
