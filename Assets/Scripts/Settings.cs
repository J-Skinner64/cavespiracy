using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Settings : MonoBehaviour
{
    public AudioMixer Mixer;

    public void SetVolume(float volume)
    {
        Mixer.SetFloat("volume", volume);
    }
}
