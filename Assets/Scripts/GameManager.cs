using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private void Awake()
    {
        Gold = PlayerPrefs.GetInt("Gold");
        potions =PlayerPrefs.GetInt("potions");
        if(GameManager.instance != null)
        {
            Destroy(gameObject);
            return;
        }
        instance = this;
    }

    //references
    public Player player;
    public FloatingTextManager floatingTextManager;
    //Logic
    public int Gold;
    public int potions;
    //floating text
    public void ShowText(string msg, int fontSize, Color color, Vector3 position, Vector3 motion, float duration)
    {
        floatingTextManager.Show(msg, fontSize, color, position, motion, duration);
    }



}
