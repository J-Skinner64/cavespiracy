using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Chest : Collectable
{
    
    public Sprite emptyChest;
    int pesosAmount;
    private int[] randomPesos = new int[] {5,10,15,20,25,30,35,40,45,50};
    public int Pesos()
    {
        return randomPesos[UnityEngine.Random.Range(0, randomPesos.Length)];
    }
    protected override void OnCollect()
    {
        if (!collected)
        {
            FindObjectOfType<AudioManager>().Play("Chest");
            collected = true;
            GetComponent<SpriteRenderer>().sprite = emptyChest;
            pesosAmount = Pesos();
            GameManager.instance.Gold += pesosAmount;
            GameManager.instance.ShowText("+" + pesosAmount.ToString() + " Gold!", 25, Color.yellow, transform.position, Vector3.up * 50, 1.5f);
            transform.localScale = new Vector3(1, 1, 1);
        }
    }
}