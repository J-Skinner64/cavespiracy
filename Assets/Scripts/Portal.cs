using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : Collidable
{
    public string[] sceneNames;
    protected override void OnCollide(Collider2D coll)
    {
        if (coll.name == "Shaun")
        {
            int Gold = GameManager.instance.Gold;
            int potions = GameManager.instance.potions;
            PlayerPrefs.SetInt("Gold", Gold);
            PlayerPrefs.SetInt("potions", potions);
            PlayerPrefs.Save();
            SceneManager.LoadScene(sceneBuildIndex:2);
        }
    }
}
